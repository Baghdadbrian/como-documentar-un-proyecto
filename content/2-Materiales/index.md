# Materiales

## Qué documentar :book:
- Aquello que nos motive: un proyecto, evento, actividad, taller, área de conocimiento o proceso de aprendizaje...
- Objetivo final: generar un archivo analógico, digital o híbrido.

## Equipo :busts_in_silhouette:
- Grupo núcleo o gestor del archivo: no tiene que ser muy numeroso pero sí comprometido. ¡Y dispuesto a cualquier tarea!

## Infraestructura :computer:
- Espacio de almacenaje físico: para organizar los materiales en función de algunos campos pactados (fecha, materia, autoría...).
- Ordenador: de mesa, portátil o tablet de potencia media.
- Internet: conexión cableada o mediante Wi-Fi.

## Herramientas de registro :pencil:
- Analógicas: cuadernos, bolígrafos y rotuladores para elaborar relatos y relatogramas (relato hecho a base de dibujos, iconografía y palabras claves).
- Digitales: cámara de foto y vídeo, grabadora de audio, software básico de procesador de textos.

## Herramientas de archivo :file_folder:
- Espacio web: propio y creado específicamente (necesitaremos nociones de programación, diseño web y/o gestión de servidores); o un sistema de gestión de contenidos o CMS con plantillas: Drupal, Wordpress, Joomla o Tumblr.
- Plataformas: que nos permiten alojar información para luego ser enlazada a nuestras páginas: Flickr, YouTube, Soundcloud...
- Software libre: para compartir archivos específicos como Zotero o Calibre.

## Normas de gestión :speech_balloon:
- Consenso sobre el contenido: qué queremos y vamos a documentar y cómo vamos a hacerlo.
- Consenso sobre el equipo: qué tareas son necesarias y quién las hace, herramientas de comunicación, cronograma, etc. Trello o Slack son herramientas que facilitan la gestión y comunicación de equipos.
