# Consejos

- _Menos es más_: No por mucho archivar, documentamos mejor nuestro proyecto. Piensa en el que se acerca por primera vez al proyecto, y también en ese o esa seguidora fiel e implicada. Sencillez y criterio, mejor que cantidad y deseo de contarlo todo.

- _Etiquetar importa_: De hecho, es lo que más importa. Si no lo puedo encontrar facilmente no existe. Y si la información no está relacionada con otra parte del archivo, es decir, puesta en contexto, parecerá un islote a la deriva en vez de la isla de un archipiélago, que es lo que es un archivo. Todo es susceptible de ser etiquetado. Y todo aquello que está etiquetado crea una jerarquía de relación y una taxonomía dentro del archivo.

- _Las licencias libres son buenas para la salud del archivo_: ¿Cómo permitir que algo sea copiable, replicable y mejorable? Las licencias libres nos permiten la creatividad y el reconocimiento del trabajo ajeno.

- _Responsive Design_: Piensa en la medida de lo posible en la accesibilidad. ¿Cómo se podrá mover una persona con dificultades auditivas o de visión?

- _Transmedia es apetecible_: Si un texto contiene un vídeo, una foto, un link y un audio, será mil veces más atractivo que uno que no. No se trata de dar lo mismo en diferentes formatos sino de complementar la información.

- _Si es bello, la gente lo disfruta_: Sin más: un bonito diseño, una tipografía legible y bien calibrada, unas imágenes optimizadas: ¿a quién no le va a gustar?

- _Cuidar del jardín_: No sólo es importante generar el archivo sino mantenerlo vivo, actualizado y sin bugs (errores muy comunes en todas las webs). Links rotos, archivos corruptos o el temido 404 Error son nuestras plagas a erradicar.
