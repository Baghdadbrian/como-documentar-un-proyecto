# 3. Documentando que es gerundio

Un buen archivo ha de ser diverso y atractivo, tanto en su contenido como en su formato: tener archivados diferentes tipos de documentos hará que eso se haga efectivo. Lo ideal es que nuestro proyecto esté documentado a base de imágenes, documentos, vídeo, audio, presentaciones y publicaciones. Imagina las piezas con diferentes formatos: desde artículos, podcast, entrevistas, post de actualización donde simplemente mantengas informada a tu comunidad o notifiques una actividad puntual a reportajes en los que consultes varias fuentes.

# 3.1. Herramientas de registro
Las relatorías textuales y gráficas (dibujos o fotos) y las crónicas son piezas frescas escritas durante y después de un evento o actividad que aportan mucha información y calidad de la experiencia de los eventos. Para redactar colectivamente los textos podéis utilizar PiratePad o Google Drive.

Para el tratamiento de fotos: Snapseed, Prisma o VSCO Cam son aplicaciones sencillas que te servirán tanto para editar tus fotos como para tomarlas. También os podéis apoyar en bancos de Fotos Libres como Raumrot, Morguefile o Wikimedia Commons.

Si tenéis la suerte de que alguien de vuestro equipo sepa o quiera hacer dibujos y rotular, podéis animaros a hacer relatogramas. Herramientas como Sketchbook te permiten después tratar los bocetos tomados durante la realización de un evento, una conferencia o una entrevista.

Las cápsulas de vídeo de creación propia son apreciadísimas en el archivo de un proyecto: es el modo más directo de transmitir a las personas que no han estado la esencia de una actividad. Imaginad que salís a hacer un paseo (ver Cómo hacer un Paseo de Jane) por vuestro barrio para conocerlo mejor a través de un itinerario marcado. El registro de esa ruta puede ser un material magnífico para situar y dar a conocer el proyecto. Recuerda que deben ser vídeos cortos (no más de tres minutos), con un hilo narrativo claro y trufados de recursos que transmitan el ambiente de la actividad.

Cualquier sistema operativo de vuestro ordenador tendrá por defecto un editor de vídeo que os permitirá crear el relato a partir de un bruto. Y para alojarlas, podéis crear una cuenta en Vimeo o YouTube con el usuario de vuestro proyecto.

Otra alternativa a los vídeos procesados son los vídeos generados a través de retransmisiones en directo, como los que nos permite hacer la herramienta para hacer streaming Periscope.

¡No te quedes en el típico texto plano editado con enlaces, negrita y cursiva, y pásate al transmedia! A continuación, os dejamos una serie de herramientas para transformar los documentos generados en una experiencia transmedia:

![](images/tabla-herramientas_de_registro.jpg)

Todas son gratuitas (en sus versiones no premium) y colaborativas, pueden usarse con la tablet o con el ordenador.

Como ves, hay muchos modos de plasmar nuestra información más allá de los formatos tradicionales. La combinación de piezas más periodísticas combinadas con audiovisual y formatos más interactivos, nos permitirán construir en nuestro gestor de contenidos o web elegido, un archivo trepidante donde podamos involucrar al usuario.

## 3.2. Herramientas de archivo
Aparte del contenido original que hemos creado, debemos archivar la información disponible ya en otros medios y/o fuentes con el fin de "curarla" y hacerla accesible. Para ello, además de los criterios que indicamos en el punto anterior, dispondremos de varios agregadores o plataformas que nos permiten construir archivos en línea y gestionarlos de manera colaborativa.

 _**Para compartir bibliografía**_

Zotero es la herramienta colaborativa estrella para recopilar, organizar, citar y compartir referencias híbridas (libros y otros materiales). Con este software de código abierto podemos crear una suerte de "sala de lectura digital" aprovechando las opciones de comentarios en grupo que ofrece la herramienta. En este enlace encontrarás una práctica guía de configuración de la herramienta.
Calibre es un software libre que nos permite gestionar y compartir bibliotecas de ebooks y, hasta con mínimos conocimientos de HTML, generar tus propios ebooks.
Asegúrate de poner a disposición pública solamente libros licenciados con licencias libres o abiertas, desde dominio público hasta anti-copyright pasando por todas las modalidades de Creative Commons.

_**Para archivar contenidos disponibles en red**_

Storify, Paper Li, Scoop.it! o Curata. Sólo necesitaréis un perfil en cualquiera de estas aplicaciones para poner en relación documentación existente en redes sociales y medios digitales. En concreto, Flipboard nos permite crear una revista informativa seleccionando materiales en función de temas y etiquetas existentes.

Son herramientas ideales para las labores de filtrado y catalogación de contenido.

_**Manejando material sensible**_

No, no nos hemos olvidado de los materiales analógicos de nuestro archivo. En el caso de nuestro proyecto asociado a la memoria del barrio, muchas fotografías antiguas en papel, mapas o documentos pueden llegar a nuestras manos. Lo mejor es que procedamos a la digitalización del material.

¿No disponemos de un scanner? No hay problema, hay buenísimas herramientas de digitalización como ScanBot que te permitirán escanear desde el móvil y pasar a formato PDF cualquier imagen o documento sensible.

A poco que le des algo de forma y agrupes la información a modo de publicación, por ejemplo: "Fotos antiguas del Barrio antes de la construcción de nuestro centro", plataformas como Issuu te permitirán después subir y alojar los documentos digitalizados y ponerlos a disposición de los demás.

El peligro de cualquier archivo es contener información desperdigada o inencontrable. Agrupando contenido mediante aplicaciones te anticipas a los riesgos de dispersión e inaccesibilidad.

## 3.3. Algunas nociones mínimas de catalogación

Bien, pues ya hemos reunido un grueso de documentación original, archivada y gestionada en colectivo en nuestro pequeño archivo analógico y digital, y llegamos al último pero no por eso menos importante punto. Ha llegado el momento cumbre de un archivo: la catalogación. La catalogación nos permite, no sólo hacer más accesible nuestro archivo (uno de nuestros principales objetivos), sino poner en relación las ideas más importantes que nuestro archivo arroja.

Para catalogar, debemos tener unas normas claras y consensuadas por el núcleo editor y conocidas y aceptadas por nuestra comunidad de colaboradores. Es muy importante que queden claras para nuestros colaboradores/as, si queremos que puedan contribuir de un modo autónomo.

_**Descripción de nuestro archivo**_

En el lugar más visible de nuestro espacio digital (web, blog) o físico debe de haber una descripción de nuestro archivo y, asimismo, de qué tipo de documentos estamos archivando. Así, la gente sabrá a qué atenerse cuando entre en nuestro archivo, tanto para "bajar" o consultar como para "subir" y contribuir con contenido.

Es bonito indicar de un modo atractivo el tipo de documentos que albergamos. Este banco de tipografías libres y este banco de iconos libres nos permitirán hacer de nuestra descripción un lugar bello y práctico.

_**Una cuestión de etiqueta**_

La taxonomía o la ciencia de la clasificación se basa en el etiquetado de todo aquello que queremos clasificar. Todo proceso de documentación crea una taxonomía a partir de aquellos elementos que archiva, siempre que estén bien etiquetados, claro. Por ejemplo, en el Jardín Botánico (hermoso archivo natural), todos los árboles están clasificados formando una taxonomía propia.

Por taxonomía también se conoce a la tecnología utilizada para la gestión eficaz de información y contenidos. Es un elemento esencial en la construcción de conocimiento y sentido (el relato que comentábamos al principio) dentro de los archivos.

Las etiquetas o tags son una o varias palabras clave que asignamos a un documento almacenado en un repositorio o archivo. Las etiquetas son, por lo tanto, metadatos ya que proporcionan información (datos) descriptiva de un dato, ya sea éste una imagen, un clip de vídeo, un archivo de texto o de audio.

A partir de un correcto etiquetado, los motores de búsqueda (externos o internos de nuestra web) logran entender dónde se encuentran las cosas y cuál es su relación entre ellas, optimizando así los recursos de los usuarios de recuperación de la información. El etiquetado y la taxonomía que genera dicha metodología son un modo orgánico y democrático de ordenar y clasificar la información que circula por nuestro archivo. El etiquetado es la base de la construcción de todo archivo colaborativo, ya que una simple etiqueta nos permite relacionar contenidos con otros documentos sobre temas o autores a través de la relación semántica, contribuyendo así a la estructuración de la información.

Si hacemos una analogía y contemplamos la relación de internet (¡gran archivo en sí mismo!) y el buscador Google (su motor de búsqueda más extendido) entenderéis enseguida (porque todos/as lo habéis experimentado) el papel crucial que juegan las palabras claves dentro de lo que se conoce como Web Semántica.

En los archivos colaborativos, las palabras clave tradicionales se pueden sustituir de una manera informal y personal por los usuarios del repositorio, creando así su propio tesauro, a veces sólo legible para esa comunidad.

Además de las etiquetas, podemos asignar a cada dato o documento de nuestro repositorio un formulario con unas serie de campos, obligatorios algunos, opcionales otros, que nos servirán para describir de manera exhaustiva un elemento.

Campos habituales en un formulario, por ejemplo, en un repositorio de libros y material audiovisual son: Título, Temática, Autor/a, Editor/a, Año, Enlaces relacionados, Licencia. Si uno de esos campos obligatorios es "Palabras clave" o "Etiquetas", con ellos te aseguras que el archivo contará con una especie de raíces semánticas que mantendrán relacionados a los documentos del repositorio. Ya lo hemos mencionado, pero uno de los pilares de cualquier archivo son las fuentes. Tanto en los contenidos creados ex profeso por nosotros, donde claro que puede haber fragmentos o inspiración que provenga de otras fuentes (no tengáis miedo a la reescritura, al mash up, pero citad, ¡siempre citad!), como por supuesto en los documentos creados por otras personas que utilizamos en el archivo, hemos de visibilizar siempre las fuentes consultadas (si son más de una y diversas, mejor que una y siempre la misma).

_**Administrador, colaboradoras y comunidad**_

En todo archivo colaborativo, como en todo proyecto colaborativo, hay una cuestión de confianza presente. Para desarrollar la confianza, que las normas y las funciones de cada uno/a estén claras, dentro de una flexibilidad, ayuda a la fluidez de los trabajos colectivos. Por ello, debe haber personas con distintos poderes sobre el archivo. Colaborativo no significa necesariamente que todas tengamos las mismas capacidades sobre un proyecto sino que el proyecto no está cerrado, no está restringido a colaboraciones.

Así, las Administradoras/Administradores de un archivo serán aquellas personas que tengan poderes totales sobre el repositorio; su poder es tal que pueden llegar a tener hasta la potestad de borrar una referencia. Sin embargo, los/as colaboradores/as o Contributors son aquellos que, una vez registrados, podrán aportar referencias con autonomía pero no acceder a otras funciones de gestión.

El uso y familiaridad con estas nociones hará que vuestro archivo desarrolle conjuntamente entre sus usarios/as unas normas de gestión y uso. Claras y visibles para todos los miembros del archivo colaborativo, tanto el núcleo gestor como los colaboradores, sólo las normas de gestión nos permitirán cuidar un archivo. La participación en un archivo colaborativo implica familiarizarse con las herramientas y conceptos en los que se basa la filosofía y metodología de catalogación del fondo del archivo en el que se participa. Y hay que realizar esta tarea siempre con cuidado y responsabilidad.

La dificultad de documentar radica muchas veces en el mantenimiento. Acordáos de la metáfora del jardín: lo más sencillo es plantar las flores y árboles, y lo más difícil, mantenerlas con vida. En nuestro jardín lo más probable es que aparezcan frecuentemente bugs (errores de software; bug también significa parásito, como las garrapatas y las pulgas). Links rotos, archivos que desaparecen o corruptos o el siempre temido 404 Error son nuestros más temidos enemigos.

Una buena idea para mantener vivo un archivo y poner cara a las personas colaboradoras son las quedadas analógicas para editar, experiencias como las Editatonas de Wikipedia o los Tagging Days de La Fanzinoteca, así nos lo confirman.

Estos encuentros además crearán comunidad, un aspecto fundamental para mantener vivo un archivo colaborativo. Siempre que puedas, crea conversación con tu comunidad, haz que tu archivo sea un pretexto para activar debates o actividades de encuentro. Para activar y mantener viva esa conversación, vuestra presencia en las redes sociales, el etiquetado con hashtags (que no son otra cosa que etiquetas y palabras clave), la mención en fotos y publicaciones a personas interesadas, la elaboración de una Lista de prescriptores o incluso la creación de una lista de correo a la que enviar una Newsletter periódica pueden ser caminos digitales para mantener viva la llama de la comunidad de vuestro archivo.

También podéis contar con la comunidad a la hora de buscar recursos materiales específicos o monetarios para una actividad puntual o para labores de mantenimiento. A veces, añadir un simpático botón de ¡Dona! o lanzarse a montar un crowdfunding en caso de ser necesario, refuerza comunidades y refrendan la labor de los equipos gestores de los proyectos.

Un archivo es como un mapa y los cartógrafos de ese mapa somos los documentalistas, es decir, los que nos ocupamos de buscar, seleccionar, organizar y hacer accesible el tesoro: la información.
