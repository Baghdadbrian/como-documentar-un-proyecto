# 2. Imaginar el relato y la estructura

Una vez clarificado el proyecto y el equipo humano y técnico con el que contamos, será el momento de empezar a pensar en el relato y la estructura de que constará nuestro archivo. Recordad que cada tipo de archivo cuenta una historia. Querer dar cuenta de uno mismo o de un grupo o de un proyecto, nos exige de algún modo construir un relato. Será el tipo de archivo el que nos lleve a elegir unas herramientas u otras con el fin de llevar a cabo más adelante las tareas de registro y catalogación.

## 2.1. ¿Qué queremos contar con nuestro archivo?
En el título del proyecto ficticio que hemos imaginado para articular esta guía hay una pregunta implícita: ¿Hay alguien ahí fuera? Es decir, ¿qué sucede alrededor de nuestro centro escolar? ¿Con quién convivimos? ¿Qué historia hay detrás del barrio de nuestro centro? Así pues, lo que queremos contar a través de nuestro proyecto de documentación es ese "exterior" a nuestro centro, esa memoria de nuestro entorno (memoria no sólo en pasado, también en presente), ese entorno a través del que muchas veces pasamos de largo.

Teniendo ese objetivo claro empezaremos a recabar información que pueda responder a nuestra pregunta. Es probable que a unos cuantos golpes de clicks o de preguntas en la calle o en casa, ya encontremos información suficiente sobre nuestro barrio como para construir un pequeño archivo: declaraciones, historias, artículos, entradas de Wikipedia, reportajes televisivos, piezas en YouTube, artículos de prensa escrita o clips de radio… Pero, entonces, ¿qué vamos a ser? ¿Meros recopiladores de información?

Como ya sabemos, no hay relato objetivo ni narrador neutro. Tampoco hay archivo inocente. Todo aquel que recaba, selecciona y media con la información, está dejando su huella. Todo archivo tiene la intención de contar algo determinado. Nuestro papel en la construcción de ese relato será, conscientemente, la de editores/as y curadores/as de contenido. ¿Y esto qué significa?

En la introducción hemos contado cómo nos encontramos en un momento de maremágnum informativo, sometidos como sociedad a constantes estímulos con datos, información e historias a las que no damos a basto para digerir diariamente. En tiempos de saturación e "infoxicación" son fundamentales los canales que median entre la información y las personas. Es por eso que la figura del editor de información o "curador" de contenidos se convierte en fundamental en cualquier archivo.

Las principales funciones de un curador de contenido (del inglés content curator), más allá de la mera búsqueda y recopilación de la información, son las de:

- Búsqueda y selección de materiales con criterio y mirada crítica filtrando lo importante de aquello que es superfluo.
- Elaboración de nuevos contenidos.
- Puesta en relación de las ideas principales que arrojan nuestros materiales.

Los criterios de utilidad, veracidad y relevancia deberán atravesar siempre estas tres funciones. Guiándonos por ellos lograremos dar valor a lo que contamos a través de nuestro archivo, y generaremos conocimiento. También hay un lema ético fundamental en la documentación que deberéis tener presente: "Bienvenida la remezcla, pero siempre citando la fuente".

También sobre los contenidos propios hemos de aplicar esas mismas funciones y criterios: no todo lo que sucede vale. De los eventos que tengan lugar durante la semana de puertas abiertas de nuestro proyecto, por ejemplo, deberemos seleccionar qué se documenta, qué es lo que realmente enriquece y favorece el aprendizaje colectivo que supone construir este archivo.

Una vez tenemos claro qué queremos contar y qué papel jugaremos dentro de la narración del relato de nuestro archivo, pasaremos a la segunda fase de este punto que aborda: la estructuración de los contenidos.

Antes de cerrar este punto me gustaría hablaros de un fenómeno muy habitual con el que se topan los documentalistas. Buscando algo, encuentran algo mucho más interesante y digamos que "se pierden por el camino". ¿A qué os suena la cantidad de horas que habéis pasado vagabundeando sin rumbo por la red? Esta suerte de deriva, que podría considerarse un obstáculo para alcanzar nuestro objetivo, acaba dando muchas veces gratificantes sorpresas. Así que, si bien es bueno tener claro aquello que queremos contar, no cerréis mucho el relato y permaneced abiertos a nuevos rumbos que, seguro, os encontraréis en esta construcción de la memoria del barrio de vuestro centro. Ya sabemos que se aprende haciendo. Y el ensayo-error forma parte de este proceso de aprendizaje que es documentar un proyecto y construir un archivo.

## 2.2. La estructura de nuestro archivo
Una vez que sabemos el qué, podemos pensar en el cómo, aunque muchas veces es el cómo el primero en marcarnos el qué. Por ejemplo, sabemos que queremos construir una biblioteca de aula para compartir bibliografía y eso nos dará una estructura y formato muy determinado: el de biblioteca digital, con secciones, temas, etc. Aún así, tal vez queramos compartir no sólo libros, sino también documentos audiovisuales, lo cual nos obligará a implementar otras herramientas. Cada archivo presenta sus propios desafíos de organización de la información. Y eso es lo bonito.

Las preguntas clave en este momento son:

- ¿Cómo imaginamos nuestro archivo digital y su diálogo con el espacio físico? ¿Cómo combinaremos la perspectiva digital con el archivo material? Recordad que nuestro archivo era híbrido: digital y analógico.
- ¿Cómo organizar la información seleccionada, los contenidos creados y la experiencia de estar haciendo el archivo? ¡Nuestro aprendizaje por el camino también es algo reseñable!
- ¿Cómo hacer nuestro archivo accesible al mayor número de personas?

Por tanto, en esta fase, nuestras funciones y tareas tendrán que ver con la gestión, la organización y la distribución de la información. La estructura de un archivo establece un hilo conductor que dota de sentido y discurso al relato. También orientará nuestro relato a un tipo de público o a otro.

En el archivo de la memoria de barrio podremos combinar el material propio generado a través de la documentación de las actividades (digital y analógico), junto con la información "curada" y filtrada de la información disponible que obtengamos en la red, bibliotecas, hemerotecas o a través de nuestras propias pesquisas.

La suma del espacio físico donde mostraremos material analógico (por ejemplo, mapas y/o fotografías) y el banco de conocimiento transmedia generado en nuestro espacio digital será nuestro archivo. Esto nos obligará a hacernos muchas preguntas: ¿qué necesitamos alojar?, ¿donde lo haremos?, ¿qué funcionalidades queremos que tengan nuestros espacios físicos y digitales?, ¿habrá un predominio de imágenes o de texto?

¿A que estas preguntas nos llevan enseguida a pensar en pestañas, ficheros, carpetas, columnas o expositores? Aquí el lápiz y el papel ayudan mucho. Es el momento de imaginar y ponerse a plasmar esa estructura en esquemas, porque enseguida llegan las…, ¡limitaciones técnicas! Si tenéis la suerte de contar con un programador/a en vuestro equipo, el cielo es el limite, pero si dependemos de las funcionalidades del CMS elegido, tendréis que adaptar vuestros sueños de organización a los condicionamientos técnicos.

Lo mismo sucederá con el espacio físico. En el centro o en algún local cercano, tendréis espacio disponible para mostrar el material recopilado y seleccionado, pero también será limitado. ¡Aunque a veces las limitaciones desatan la imaginación y la escasez el ingenio! Tened siempre presente que, como todo proyecto experimental, este archivo será un prototipo, por lo que estará abierto a continuas mejoras y aportes sucesivos. Bajad las expectativas y el perfeccionismo y todo irá mucho mejor.

Una vez tenemos imaginado y diseñado nuestro esqueleto, y adaptado a las limitaciones tećnicas y físicas correspondientes, es hora de conocer y familiarizarnos con las herramientas de registro y archivo que os ayudarán a materializar vuestros contenidos de una manera atractiva y accesible. Además, la adquisición de las competencias digitales en el uso de estas herramientas serán un rico camino de aprendizaje.
