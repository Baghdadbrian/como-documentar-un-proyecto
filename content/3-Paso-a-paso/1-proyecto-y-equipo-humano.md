# 1. Proyecto y equipo humano

Salvo este paso previo, que ha de ser necesariamente el primero y en el que clarifiquéis vuestro objetivo y el equipo humano disponible, los demás pasos pueden solaparse y simultanearse en el tiempo, es decir, no son fases necesariamente consecutivas. Podemos seguir pensando en el archivo que queremos exactamente mientras realizamos tareas de registro y catalogación, por ejemplo. Porque es cierto que cada proyecto pide y acaba generando un tipo de archivo, y sólo encontraremos esas claves cuando lo pongamos en marcha. Aunque también nos ayudará tener claras algunas cosas antes de comenzar a acometer las muchas decisiones que debemos tomar. ¡Y es divertido organizarse!

## 1.1. Creación del proyecto
Para esta guía vamos a partir de un proyecto imaginario pero que nos puede dar claves para entender cómo se construye una archivo. Es un proyecto trasladable a cualquiera de vuestras comunidades escolares. Lo hemos titulado: ¿Hay alguien ahí fuera? Memoria y presente del barrio de mi Centro Escolar. El archivo será híbrido (digital y analógico) y su objetivo será recoger y seleccionar información para generar un relato acerca del barrio en el que se encuentra nuestro centro escolar.

Vamos a partir de la hipótesis de que, para dar a conocer el proyecto, durante una semana se celebran en nuestro centro unas "Jornadas de Memoria del barrio" donde participa toda la comunidad escolar y las puertas del centro estarán puntualmente abiertas tanto a visitantes como a salidas dinamizadas para conocer y obtener información del barrio.

Será durante esa semana cuando se recoja el grueso de la información relevante, pero el archivo permanecerá abierto durante todo el curso a contribuciones, actividades extensivas y referencias complementarias. Por lo que en este sentido, el archivo también es mixto:

- recoge las vivencias de un proyecto puntual: la semana de puertas abiertas;
- también abarca desde antes: podemos empezar a contar un tiempo antes nuestro proceso y utilizar el archivo también como herramienta de difusión del proyecto;
- y llega hasta el tiempo que queramos extendernos posteriormente: de hecho, puede ser un archivo destinado a permanecer, ya que cursos sucesivos y personas que se acerquen posteriormente podrían seguir aportando.
Desde el punto de vista de los usuarios también es mixto: combinará un núcleo editor que actuará como administrador y gestor de las contribuciones del resto de usuarios, que actuarán como colaboradores.

Éste es sólo un modelo de proyecto que nos servirá para establecer los pasos de una manera situada en esta guía. La idea es que podáis aplicar los pasos que enumeramos a continuación a cualquier proyecto que requiera ser documentado o que sea susceptible de generar un archivo: desde un concurso literario hasta un viaje cultural, pasando por entornos personales de aprendizaje (PLES) compartidos o colectivos.

![](images/documentar_paso_a_paso.jpg)

## 1.2 Conformar el equipo humano y técnico
¿Quién se compromete a crear un archivo? Nuestro objetivo es crear un archivo colectivo (entre varias personas) y colaborativo (gestionado colectivamente y en abierto). Por tanto habrá diferentes tipos de colaboradores, aunque es importante conformar un núcleo previo, que será el núcleo gestor. Imaginemos que el archivo que vamos a crear es un jardín: necesitaremos jardineros/as fijos que se encarguen regularmente de plantar, podar y mantener las plantas (los documentos) vivos, aunque también sean bienvenidos aportes de colaboradores que puedan traer de vez en cuando árboles y plantas al archivo colectivo.

Una vez que sepamos quiénes se suben a este barco podemos hacer una lista de los saberes que tenemos a nuestra disposición, una especie de organigrama de equipo. En ese organigrama se deben establecer con el máximo detalle posible los saberes de cada cual y las tareas que puede desempeñar. En este momento previo también podemos elaborar un inventario con el material técnico que tenemos disponible.

Por ejemplo, tenemos alguien que tiene micro, grabadora y conocimientos de Audacity (software de edición de sonido) pero necesitamos algún redactor o una buena cámara para registrar imagen. Una vez sepamos con qué contamos también podemos establecer claramente qué necesidades podríamos llegar a tener y solicitar colaboradores. La búsqueda de saberes puede constituir un buen medio para dar a conocer el proyecto antes de que se realice mediante una llamada a la colaboración.

Los saberes que necesitamos y que iremos adquiriendo con la práctica de la creación del archivo están asociados a dos patas:

Creación de relato: curación de contenido, elección de herramientas.
Registro y catalogación: diseño de la arquitectura del archivo, captura y creación de contenidos, almacenaje y organización de materiales, etiquetado y mantenimiento.
Es importante que cada cual sepa en qué parte está o qué pata sujeta, pero el reto más gratificante es que durante la construcción del archivo todas las personas que participemos aprendamos a hacer de todo, si no de un modo especializado, sí con la suficiente familiaridad para contribuir a cada una de las tareas y funciones que nos requiera el proceso de documentación.

Para que cualquier equipo funcione es importante tener claro el reparto de tareas y roles dentro del mismo, para lo que necesitaréis canales de comunicación fluidos (presenciales y digitales) que fomenten la interdependecia y el equilibrio. Uno de los peligros mayores dentro del trabajo colectivo es que unos pocos tienden a asumir mayor carga de trabajo. Sed conscientes de ello y tratad de revisar las tareas periódicamente para reequilibrar esas cargas.

Asimismo habréis de estar atentos a las dinámicas de género que suelen imponerse en todo trabajo colectivo y generar una división del trabajo desigual: es habitual que las chicas tiendan a situarse en posiciones más subordinadas o a hacer trabajos "reproductivos" o de cuidado (por ejemplo, moderar asambleas, mediar en los conflictos, asumir la comunicación interna...) y los chicos a tener papeles considerados de mayor relevancia y con mayor visibilidad pública (por ejemplo, extenderse en el uso de la palabra, tomar protagonismo en presentaciones...).

Sed conscientes de que estas inercias son más fuertes que nosotras y nosotros porque se derivan de estructuras muy asentadas en la sociedad.
