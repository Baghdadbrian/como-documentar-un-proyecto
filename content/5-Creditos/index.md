#Créditos
Esta guía fue tomada del sitio:
http://laaventuradeaprender.educalab.es/guias/como-documentar-un-proyecto

**Autores:**

Esta guía ha sido elaborada por Silvia Nanclares (@silvink), escritora, columnista e investigadora cultural. Desarrolla, desde el ámbito de la escritura, proyectos teatrales, audiovisuales, literarios y de formación artística. Ha intervenido en la creación, producción y difusión de contenidos para diferentes centros e instituciones culturales, así como en investigaciones colectivas como #bookcamping, biblioteca digital colaborativa que reseña y enlaza recursos licenciados en su mayoría con licencias abiertas. Actualmente forma parte de la cooperativa de género y comunicación Pandora Mirabilia.

**Licencia**

Esta guía está publicada bajo la siguiente licencia de uso Creative Commons: CC-BY-SA 3.0.Reconocimiento – CompartirIgual (by-sa): que permite compartir, copiar y redistribuir el material en cualquier medio o formato, así como adaptar, remezclar, transformar y crear a partir del material, siempre que se reconozca la autoría del mismo y se utilice la misma licencia de uso.

http://laaventuradeaprender.educalab.es/licencia
