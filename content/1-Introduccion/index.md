# Introducción

## La memoria del mundo

¿Cómo podemos, una vez realizada una actividad o proceso de aprendizaje, dejar una huella significativa de lo que hemos hecho y aprendido? ¿O cómo podríamos asimismo compartir los referentes que nos han inspirado para iniciar nuestro proyecto, o compartir el proceso de creación y producción previo o posterior asociado a una actividad?

Queremos poner de relieve la importancia de la documentación en todo proyecto que llevemos a cabo, ya sea de un proceso que se extienda en el tiempo o de una actividad puntual. En esta guía encontrarás también las herramientas y pistas necesarias para hacer de un proceso de documentación una experiencia de aprendizaje y comunicación satisfactoria.

Porque, inevitablemente, cuando documentamos estamos creando una versión de los hechos: una historia, nuestra historia.

## Documentación y archivo
Documentar es un verbo transitivo: siempre documentamos algo. Y el resultado de esa serie de prácticas de documentación asociadas a un acontecimiento o proceso —desde la Semana de la Cultura a una bibliografía compartida por un aula o un curso—, siempre es un archivo —ya sea el álbum de fotos en papel fotogŕafico de la boda de tus abuelos o una lista de libros favoritos en una plataforma como Goodreads.

Seguro que el verbo documentar es algo que estáis habituados a escuchar en vuestras asignaturas o procesos de aprendizaje. Es imprescindible documentarse para acreditar los saberes. Por eso muchas veces se valora positivamente que citemos las referencias bibliográficas con las que hemos trabajado o se nos penaliza cuando abusamos del corta y pega de ciertas fuentes como Wikipedia. No se trata de no usarlas sino de transformar y manipular la documentación que nos proporcionan las fuentes en un contenido que muestre nuestro punto de vista hasta convertirse en un nuevo documento. Documentar siempre nos exige buscar, filtrar y seleccionar información para poder contrastar puntos de vista y poder aportar un relato de las vivencias o los aprendizajes experimentados.

La esencia de la documentación y su objetivo (la creación de un archivo) es poner a disposición de los demás documentos, ya sean textos, material audiovisual —o incluso objetos físicos en el caso de un archivo analógico—, que den cuenta de la realización de un proyecto.

## El archivo más famoso del mundo
La institución documental o el ejemplo de archivo más reconocible y reconocido por toda la sociedad —tal vez por ser el más antiguo y respetado— es la biblioteca. Todos sabemos para qué sirve y cómo comportarnos en una biblioteca.

Pero para generar esa familiaridad y esa accesibilidad han hecho falta años de perfeccionamiento en el arte y la disciplina de la documentación y la biblioteconomía. Pensadlo cuando volváis a entrar en contacto con una biblioteca.

## Los archivos digitales llegaron para quedarse
La documentación y las prácticas de archivo son hoy, gracias o por culpa de internet, más que nunca parte consustancial de nuestra vida. La red, ese inacabable archivo alimentado por tantísimas personas, está presente en nuestra cotidianidad de tal manera que casi ya nos cuesta imaginar nuestra vida sin ella. E internet en sí mismo no deja de ser un gran archivo de doble dirección: cuando subimos información nos convertimos en documentalistas y cuando la "bajamos", en usuarios de ese archivo.

Por ejemplo, cuando elaboramos un álbum de fotos en Facebook, una lista de canciones en Spotify o una de vídeos en YouTube estamos documentando una experiencia y generando una suerte de archivo dentro de ese otro gran archivo que es la red. Pero lo más interesante de esos archivos que vamos alimentando no es sólo que cuentan muchas cosas sobre quiénes somos o la imagen que construimos de cara al mundo —todo archivo cuenta una historia—, sino que son archivos abiertos al aporte de nuestros "amigos" o "seguidores" en las redes.

Pero el acceso digital a la cada vez más ingente cantidad de material nos desafía a reflexionar en torno a las prácticas de archivo. ¿Qué guardar? ¿Cómo compartir? El poeta norteamericano y fundador del archivo digital UbuWeb Kenneth Goldsmith nos advierte: "En tiempos de redes, el archivo es el nuevo olvido". Necesitamos pararnos a pensar acerca de cuánto estamos dispuestos a admitir que podemos almacenar, digerir y compartir.

![](images/documentar_01.jpg)

Porque, en un punto, el trabajo de archivo parece infinito. Sobre todo cuando queremos documentar actividades o proyectos realizados en colectivo. Las herramientas de archivo colaborativo —como veremos también en esta guía—nos permiten abordar esta a veces inabarcable tarea de clasificar, catalogar y compartir en red la cantidad de información y conocimiento que estamos generando casi constantemente.

Esta guía plantea un espacio para la reflexión y el conocimiento sobre lo que significa documentar hoy día y cómo podemos hacerlo de un modo más razonable y gratificante.

Así pues, la complejidad se dispara cuando somos varias las personas al cuidado de un archivo o cuando, además, queremos que nuestro archivo esté abierto a colaboraciones externas, es decir, cuando creamos un archivo colaborativo. Los archivos abiertos nos permiten mejorar la diversidad de la información que recabamos y la riqueza en los puntos de vista, además de depararnos sorpresas y ser más democráticos per se.

## ¿Y si no lo documento no existe?
¡No, ni mucho menos! De hecho, queremos reivindicar el derecho a no-documentar, a mantener las vivencias dentro de las posibilidades y la potencia del recuerdo y la memoria oral. Recuperar la relación directa con los sucesos y los procesos sin la necesidad compulsiva de contarlos y hacerlos públicos. Eso sí, en el momento en que nos decidimos a elaborar y compartir la memoria de alguna actividad o experiencia significativa para nosotros, es importante saber cómo hacerlo.

Esperamos que esta guía os sea útil en ese camino.
