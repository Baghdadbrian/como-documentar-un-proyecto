Cómo documentar un proyecto

![](images/portada_Como-documentar-un-proyecto.jpg)

La guía La Aventura de Aprender sobre **'Cómo documentar un proyecto'** nos invita a reflexionar, por un lado, sobre cómo activar la memoria y la continuación de la vida de las actividades y procesos que realicemos, y por otro, qué medios, de los que tenemos a nuestro alcance, pueden ser más idóneos para conseguirlo.

Esta guía ha sido elaborada por *Silvia Nanclares (@silvink) en Twitter*, escritora, columnista e investigadora cultural. Desarrolla, desde el ámbito de la escritura, proyectos teatrales, audiovisuales, literarios y de formación artística. Ha intervenido en la creación, producción y difusión de contenidos para diferentes centros e instituciones culturales, así como en investigaciones colectivas como #bookcamping, biblioteca digital colaborativa que reseña y enlaza recursos licenciados en su mayoría con licencias abiertas. Actualmente forma parte de la cooperativa de género y comunicación Pandora Mirabilia.
